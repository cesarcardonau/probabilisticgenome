# import sys,os,errno
from Bio import SeqIO
import subprocess
import pandas as pd

#INPUT FILES
CENTROID_FASTA="bacteria-centroids.fasta"
PATRIC_GENOMES_FASTA='patric-16S.fasta'
PATRIC_GENOMES_MAP='patric-16S.taxonomy'
GG_GENOMES_FASTA='gg138_99.fasta'
GG_GENOMES_MAP='gg138_99.taxonomy'

#INTERMEDIATE FILES
CENTROIDS_GG_BLAST="bacteria-centroids.gg.blast"
CENTROIDS_PATRIC_BLAST="bacteria-centroids.patric.blast"

#OUTPUT FILES
PATRIC_GENOMES_SELECTED_MAP='patric-16S-selected.map'
PATRIC_GENOMES_SELECTED_FASTA='patric-16S-selected-per-taxaid.fasta'
PATRIC_GENOMES_SELECTED_BLAST='patric-16S-selected-per-taxaid.blast'

MIN_TAXONOMIC_LEVEL=3 #Class
MAX_TOP_RANK_GG_SELECTED=1 #Only the top 1 GG hit is consider groud truths

def check_file(filename):
    with open(filename, 'r') as f:
        head = [next(f) for x in xrange(2)]
    for column in head:
        print(column[:150])
    
def run_blastn80_30(fp_query,db_name,fp_out):
    blastn_cmd='/software/blastplus-2.2-el6-x86_64/bin/blastn -query {0} -max_target_seqs 30 -strand both \
    -task blastn -db {1} -perc_identity 80 -outfmt 7 -out {2}'.format(fp_query,db_name,fp_out )
    BLAST_process = subprocess.Popen(blastn_cmd,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    BLAST_process.wait()
    return(fp_out)
def run_blastn90(fp_query,db_name,fp_out):
    blastn_cmd='/software/blastplus-2.2-el6-x86_64/bin/blastn -query {0} -strand both \
    -task blastn -db {1} -perc_identity 90 -outfmt 7 -out {2}'.format(fp_query,db_name,fp_out )
    BLAST_process = subprocess.Popen(blastn_cmd,shell=True,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    BLAST_process.wait()
    return(fp_out)

def run_pg_batch(params):
    print(params)

#check files
#check_file(CENTROID_FASTA)
#check_file(GG_GENOMES_FASTA)
#check_file(GG_GENOMES_MAP)
#check_file(PATRIC_GENOMES_FASTA)
#check_file(PATRIC_GENOMES_MAP)

#
